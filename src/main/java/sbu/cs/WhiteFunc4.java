package sbu.cs;

public class WhiteFunc4 implements WhiteFunction {
    @Override
    public String func(String inp1, String inp2) {
        return inp1.length() % 2 == 0 ? inp1 : inp2;
    }
}
