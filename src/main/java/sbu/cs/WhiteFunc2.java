package sbu.cs;

public class WhiteFunc2 implements WhiteFunction {
    @Override
    public String func(String inp1, String inp2) {
        return inp1 + new BlackFunc1().func(inp2);
    }
}
