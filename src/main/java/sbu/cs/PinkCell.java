package sbu.cs;

public class PinkCell extends Cell {
    private String upInput = null;
    private String leftInput = null;
    private String output = null;
    private int funcNum = 0;

    public PinkCell(int funcNum) {
        this.funcNum = funcNum;
    }

    @Override
    public void setUpInput(String upInput) {
        this.upInput = upInput;
    }

    @Override
    public void setLeftInput(String leftInput) {
        this.leftInput = leftInput;
    }

    @Override
    public String getOutput() {
        switch (funcNum){
            case 1:
                output = new WhiteFunc1().func(leftInput, upInput);
                break;
            case 2:
                output = new WhiteFunc2().func(leftInput, upInput);
                break;
            case 3:
                output = new WhiteFunc3().func(leftInput, upInput);
                break;
            case 4:
                output = new WhiteFunc4().func(leftInput, upInput);
                break;
            case 5:
                output = new WhiteFunc5().func(leftInput, upInput);
                break;
        }
        return output;
    }
}

