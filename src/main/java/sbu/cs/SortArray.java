package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {
        for (int i = 0; i < size; i++) {
            int jMin = i;
            for (int j = i + 1; j < size; j++) {
                if(arr[j] < arr[jMin]){
                    jMin = j;
                }
            }
            if (jMin != i){
                int temp = arr[i];
                arr[i] = arr[jMin];
                arr[jMin] = temp;
            }
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {
        for (int i = 1; i < size; i++) {
            int temp = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > temp){
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = temp;
        }
        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        MergeSort mergeSort = new MergeSort(arr);
        mergeSort.mergeSort(0, size - 1);
        return mergeSort.getArr();
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int l = 0, h = arr.length - 1;
        while (l < h){
            int m = (l + h)/2;
            if (arr[m] == value){
                return m;
            }else if (value < arr[m]){
                h = m;
            }else {
                l = m + 1;
            }
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        BinarySearch binarySearch = new BinarySearch();
        binarySearch.binarySearch(arr, value);
        return binarySearch.getLoc();
    }
}
