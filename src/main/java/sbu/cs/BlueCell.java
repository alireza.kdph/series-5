package sbu.cs;

public class BlueCell extends Cell {
    private String upInput = null;
    private String leftInput = null;
    private String downOutput = null;
    private String rightOutput = null;
    private int funcNum = 0;

    public BlueCell(int funcNum) {
        this.funcNum = funcNum;
    }
    @Override
    public void setUpInput(String upInput) {
        this.upInput = upInput;
    }

    @Override
    public void setLeftInput(String leftInput) {
        this.leftInput = leftInput;
    }

    @Override
    public String getDownOutput() {
        switch (funcNum){
            case 1:
                downOutput = new BlackFunc1().func(upInput);
                break;
            case 2:
                downOutput = new BlackFunc2().func(upInput);
                break;
            case 3:
                downOutput = new BlackFunc3().func(upInput);
                break;
            case 4:
                downOutput = new BlackFunc4().func(upInput);
                break;
            case 5:
                downOutput = new BlackFunc5().func(upInput);
                break;
        }
        return downOutput;
    }
    @Override
    public String getRightOutput() {
        switch (funcNum){
            case 1:
                rightOutput = new BlackFunc1().func(leftInput);
                break;
            case 2:
                rightOutput = new BlackFunc2().func(leftInput);
                break;
            case 3:
                rightOutput = new BlackFunc3().func(leftInput);
                break;
            case 4:
                rightOutput = new BlackFunc4().func(leftInput);
                break;
            case 5:
                rightOutput = new BlackFunc5().func(leftInput);
                break;
        }
        return rightOutput;
    }
}

