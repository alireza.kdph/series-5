package sbu.cs;

public class BlackFunc5 implements BlackFunction {
    @Override
    public String func(String inp) {
        String output = "";
        for (int i = 0; i < inp.length(); i++) {
            output = output + (char)('a' + 'z' - inp.charAt(i));
        }
        return output;
    }
}
