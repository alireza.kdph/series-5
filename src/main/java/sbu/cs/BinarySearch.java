package sbu.cs;

import java.util.Arrays;

public class BinarySearch {
    private int loc = -1;
    public void binarySearch(int[] arr, int value){
        Arrays.sort(arr);
        search(arr,0,arr.length - 1,value);
    }
    private void search(int[] arr, int l, int h, int value){
        if (l < h) {
            int m = (l + h) / 2;
            if (arr[m] == value) {
                loc = m;
                return;
            } else if (value < arr[m]) {
                search(arr, l, m, value);
            } else {
                search(arr, m + 1, h, value);
            }
        }
    }

    public int getLoc() {
        return loc;
    }
}
