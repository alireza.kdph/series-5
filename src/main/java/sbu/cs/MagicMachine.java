package sbu.cs;

public class MagicMachine {
    private int n;
    private int[][] machine;
    private String input;
    private Cell[][] cells = null;
    public MagicMachine(int n, int[][] machine, String input) {
        this.n = n;
        cells = new Cell[n][n];
        this.machine = machine;
        this.input = input;
    }
    public String machineOutput(){
        machinePainting();
        int length = machine.length;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                //cell is green or yellow
                if (i == 0 && j == 0){
                    cells[i][j].setInput(input);
                    continue;
                }
                else if (cells[i][j] instanceof GreenCell || cells[i][j] instanceof YellowCell) {
                    if (i == 0) {
                        cells[i][j].setInput(cells[i][j - 1].getOutput());
                        continue;
                    }
                    if (j == 0) {
                        cells[i][j].setInput(cells[i - 1][j].getOutput());
                        continue;
                    }
                }
                // cell is blue
                if (cells[i][j] instanceof BlueCell){
                    if (i == 1){
                        cells[i][j].setUpInput(cells[i - 1][j].getOutput());
                        cells[i][j].setLeftInput(cells[i][j - 1].getRightOutput());
                    }
                    else if (j == 1){
                        cells[i][j].setUpInput(cells[i - 1][j].getDownOutput());
                        cells[i][j].setLeftInput(cells[i][j - 1].getOutput());
                    }
                    else {
                        cells[i][j].setUpInput(cells[i - 1][j].getDownOutput());
                        cells[i][j].setLeftInput(cells[i][j - 1].getRightOutput());
                    }
                    continue;
                }
                //cell is pink
                if (cells[i][j] instanceof PinkCell){
                    if (j == length - 1 && i != length - 1){
                        cells[i][j].setUpInput(cells[i - 1][j].getOutput());
                        cells[i][j].setLeftInput(cells[i][j - 1].getRightOutput());

                    }
                    else if (i == length - 1 && j != length - 1){
                        cells[i][j].setUpInput(cells[i - 1][j].getDownOutput());
                        cells[i][j].setLeftInput(cells[i][j - 1].getOutput());
                    }
                }
            }
        }
        cells[length - 1][length - 1].setLeftInput(cells[length - 1][length - 2].getOutput());
        cells[length - 1][length - 1].setUpInput(cells[length - 2][length - 1].getOutput());
        return cells[length - 1][length - 1].getOutput();
    }

    private void machinePainting() {
        int length = machine.length;
        for (int i = 0; i < length - 1; i++) {
            //cell is green
            cells[0][i] = new GreenCell(machine[0][i]);
            cells[i][0] = new GreenCell(machine[i][0]);
            //cell is pink
            cells[length - 1][i + 1] = new PinkCell(machine[length - 1][i + 1]);
            cells[i + 1][length - 1] = new PinkCell(machine[i + 1][length - 1]);
        }
        for (int i = 1; i < length - 1; i++) {
            for (int j = 1; j < length - 1; j++) {
                //cell is blue
                cells[i][j] = new BlueCell(machine[i][j]);
            }
        }
        //cell is yellow
        cells[0][length - 1] = new YellowCell(machine[0][length - 1]);
        cells[length - 1][0] = new YellowCell(machine[length - 1][0]);
    }
}
