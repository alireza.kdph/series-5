package sbu.cs;

public class MergeSort {
    private int[] arr;

    public MergeSort(int[] arr) {
        this.arr = arr;
    }

    public void mergeSort(int l, int h) {
        if (l < h) {
            int m = (l + h) / 2;
            mergeSort(l, m);
            mergeSort(m + 1, h);
            merge(l, m, h);
        }

    }
    public void merge(int l,int m,int h){
        int n1 = m - l + 1;
        int n2 = h - m;
        int[] left = new  int[n1];
        int[] right = new int[n2];

        for (int i = 0; i < n1; i++) {
            left[i] = arr[l + i];
        }
        for (int j = 0; j < n2; j++) {
            right[j] = arr[m + 1 + j];
        }
        int i = 0,j = 0;
        int k = l;
        while (i < n1 && j < n2){
            if (left[i] <= right[j]){
                arr[k] = left[i];
                i++;
            }
            else {
                arr[k] = right[j];
                j++;
            }
            k++;
        }
        while (i < n1){
            arr[k] = left[i];
            i++;
            k++;
        }
        while (j < n2){
            arr[k] = right[j];
            j++;
            k++;
        }
    }
    public int[] getArr(){
        return arr;
    }
}
