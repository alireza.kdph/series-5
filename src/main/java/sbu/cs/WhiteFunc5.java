package sbu.cs;

public class WhiteFunc5 implements WhiteFunction {
    @Override
    public String func(String inp1, String inp2) {
        String output = "";
        for (int i = 0; i < Math.min(inp1.length(), inp2.length()); i++) {
            output = output + (char)(((inp1.charAt(i) + inp2.charAt(i) - 2 *'a') % 26)+ 'a');
        }
        if (inp1.length() > inp2.length()){
            for (int i = Math.min(inp1.length(), inp2.length()); i < inp1.length(); i++) {
                output = output + inp1.charAt(i);
            }
        }else if (inp1.length() < inp2.length()){
            for (int i = Math.min(inp1.length(), inp2.length()); i < inp2.length(); i++) {
                output = output + inp2.charAt(i);
            }
        }
        return output;
    }
}
