package sbu.cs;

public class BlackFunc2 implements BlackFunction {
    @Override
    public String func(String inp) {
        for (int i = 0; i < inp.length(); i = i + 2) {
            inp = inp.substring(0, i + 1) + inp.charAt(i) + inp.substring(i + 1);
        }
        return inp;
    }
}
