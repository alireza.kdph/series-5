package sbu.cs;

public class BlackFunc4 implements BlackFunction {
    @Override
    public String func(String inp) {
        String output = "";
        output += inp.charAt(inp.length() - 1);
        for (int i = 0; i < inp.length() - 1; i++) {
            output = output + inp.charAt(i);
        }
        return output;
    }
}
