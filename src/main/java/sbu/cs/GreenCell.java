package sbu.cs;

public class GreenCell extends Cell {
    private String input = null;
    private String output = null;
    private int funcNum = 0;

    public GreenCell(int funcNum) {
        this.funcNum = funcNum;
    }

    @Override
    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public String getOutput() {
        switch (funcNum){
            case 1:
                output = new BlackFunc1().func(input);
                break;
            case 2:
                output = new BlackFunc2().func(input);
                break;
            case 3:
                output = new BlackFunc3().func(input);
                break;
            case 4:
                output = new BlackFunc4().func(input);
                break;
            case 5:
                output = new BlackFunc5().func(input);
                break;
        }
        return output;
    }
}
